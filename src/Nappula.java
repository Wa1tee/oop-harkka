public class Nappula {
	//koordinaatit
	private int x; //0-7 k��ntyy muotoon a-h
	private int y; //0-7 k��ntyy muotoon 1-8
	private boolean valkoinen;
	private Tyyppi t;
	private boolean valkoinenRuutu;
	
	public Nappula(){
		this.x = 0;
		this.y = 0;
		this.valkoinen = true;
		this.t = Tyyppi.RUUTU;
		this.valkoinenRuutu = true;
	}
	public Nappula(int x, int y, boolean valkoinen, Tyyppi t, boolean v){
		this.x = x;
		this.y = y;
		this.valkoinen = valkoinen;
		this.t = t;
		this.valkoinenRuutu = v;
	}
	public int annaX(){
		return x;
	}
	public int annaY(){
		return y;
	}
	public Tyyppi annaTyyppi(){
		return this.t;
	}
	public void asetaTyyppi(Tyyppi t){
		this.t = t;
	}
	
	public boolean onkoValkoinen(){
		if(t ==Tyyppi.RUUTU){
			return valkoinenRuutu;
		}else{
			return valkoinen;
		}
	}

	public void asetaVari(boolean w){
		this.valkoinen = w;
	}
	
	public boolean siirto(int x, int y){
		switch(this.t){
		case SOTILAS:
			return sotilasSiirto(x, y);
		case TORNI:
			return torniSiirto(x, y);
		case RATSU:
			return ratsuSiirto(x, y);
		case LAHETTI:
			return lahettiSiirto(x, y);
		case KUNINGATAR:
			return kuningatarSiirto(x, y);
		case KUNINGAS:
			return kuningasSiirto(x, y);
		case RUUTU:
			 return false;
		default:
			 return false;
		}

	}
	
	private boolean sotilasSiirto(int x, int y){
		if(valkoinen){
			if (this.x == x) {
				if (this.y +1 == y) {
					return true;
				}else{
					if(this.y == 1){
						if(this.y +2 == y){
							return true;
						}
					}
				}
			}
		}else{
			if (this.x == x) {
				if (this.y -1 == y) {
					return true;
				}else{
					if(this.y == 6){
						if(this.y -2 == y){
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	private boolean torniSiirto(int x, int y){
		if(this.x == x){
			return true;
		}
		if(this.y == y){
			return true;
		}
		return false;
	}

	private boolean ratsuSiirto(int x, int y){
		// alasp�in
		if (x == this.x +1 && y == this.y +2) {
			return true;					
		}									
		if (x == this.x +2 && y == this.y +1) {
			return true;					
		}									
		if (x == this.x +1 && y == this.y -2) {
			return true;					
		}									
		if (x == this.x +2 && y == this.y -1) {
			return true;					
		}
		//yl�sp�in 
		if (x == this.x -1 && y == this.y +2) {										
			return true;
		}
		if (x == this.x -2 && y == this.y +1) {
			return true;
		}
		if (x == this.x -1 && y == this.y -2) {
			return true;
		}
		if (x == this.x -2 && y == this.y -1) {
			return true;
		}
		return false;
	}


	private boolean lahettiSiirto(int x, int y){
		//KORJAA T��
		if (Math.abs(this.x - x) == Math.abs(this.y - y)) {
			return true;
		}

		return false;
	}
	private boolean kuningatarSiirto(int x, int y){
		
		if (Math.abs(this.x - x) == Math.abs(this.y - y)) {
			return true;
		}

		if(this.x == x){
			return true;
		}

		if(this.y == y){
			return true;
		}
		return false;
	}
	//tarkastaa kuningas-tyyppisen nappulan lailliset siirtymiset
	private boolean kuningasSiirto(int x, int y){

		if(		(x == (this.x +1) && y == (this.y +1))	|| 
				(y == (this.x +1) && y == (this.y   )) 	||
				(x == (this.x +1) && y == (this.y -1)) 	||
				(x == (this.x   ) && y == (this.y +1)) 	||
				(x == (this.x   ) && y == (this.y -1)) 	||
				(x == (this.x -1) && y == (this.y +1)) 	||
				(x == (this.x -1) && y == (this.y   ))	||
				(x == (this.x -1) && y == (this.y -1))){
			return true;
		}else{
			return false;
		}
	}
}
