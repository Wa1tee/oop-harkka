import java.util.ArrayList;

public class Pelilauta {
	private ArrayList<ArrayList<Nappula>> lauta;

	//konstruktori alustaa pelilaudan shakin alkutilanteeseen
	public Pelilauta(){
		ArrayList<Nappula> tyhjarivi1 = new ArrayList<Nappula>(8);
		ArrayList<Nappula> tyhjarivi2 = new ArrayList<Nappula>(8);
		ArrayList<Nappula> tyhjarivi3 = new ArrayList<Nappula>(8);
		ArrayList<Nappula> tyhjarivi4 = new ArrayList<Nappula>(8);
		ArrayList<Nappula> sotilasRivi1 = new ArrayList<Nappula>(8);
		ArrayList<Nappula> sotilasRivi2 = new ArrayList<Nappula>(8);
		ArrayList<Nappula> upseeriRivi1 = new ArrayList<Nappula>(8);
		ArrayList<Nappula> upseeriRivi2 = new ArrayList<Nappula>(8);
		this.lauta = new ArrayList<ArrayList<Nappula>>(8);
		boolean whitebox = true;
		boolean white = true;

		upseeriRivi1.add(new Nappula(0, 0, white, Tyyppi.TORNI, false));
		upseeriRivi1.add(new Nappula(1, 0, white, Tyyppi.RATSU, true));
		upseeriRivi1.add(new Nappula(2, 0, white, Tyyppi.LAHETTI, false));
		upseeriRivi1.add(new Nappula(3, 0, white, Tyyppi.KUNINGATAR, true));
		upseeriRivi1.add(new Nappula(4, 0, white, Tyyppi.KUNINGAS, false));
		upseeriRivi1.add(new Nappula(5, 0, white, Tyyppi.LAHETTI, true));
		upseeriRivi1.add(new Nappula(6, 0, white, Tyyppi.RATSU, false));
		upseeriRivi1.add(new Nappula(7, 0, white, Tyyppi.TORNI, true));
		this.lauta.add(upseeriRivi1);

		for(int i=0;i<8;i++){
			sotilasRivi1.add(new Nappula(i, 1, white, Tyyppi.SOTILAS, whitebox));
			if(whitebox){
				whitebox = false;
			}else{
				whitebox = true;
			}
		}
		this.lauta.add(sotilasRivi1);
		white = false;


		
		for (int i=0; i<8; i++) {
			tyhjarivi1.add(new Nappula(i, 2, white, Tyyppi.RUUTU, white));
			tyhjarivi2.add(new Nappula(i, 3, !white, Tyyppi.RUUTU, !white));
			if(white){
				white = false;
			}else{
				white = true;
			}
		}
		this.lauta.add(tyhjarivi1);
		this.lauta.add(tyhjarivi2);

		for (int i=0; i<8; i++) {
			tyhjarivi3.add(new Nappula(i, 4, white, Tyyppi.RUUTU, white));
			tyhjarivi4.add(new Nappula(i, 5, !white, Tyyppi.RUUTU, !white));
			if(white){
				white = false;
			}else{
				white = true;
			}
		}
		this.lauta.add(tyhjarivi3);

		this.lauta.add(tyhjarivi4);
		
		white = false;
		
		whitebox = false;
		for(int i=0;i<8;i++){
			sotilasRivi2.add(new Nappula(i,6, white, Tyyppi.SOTILAS, whitebox));
			if(whitebox){
				whitebox = false;
			}else{
				whitebox = true;
			}
		}
		this.lauta.add(sotilasRivi2);

		upseeriRivi2.add(new Nappula(0, 7, white, Tyyppi.TORNI, true));
		upseeriRivi2.add(new Nappula(1, 7, white, Tyyppi.RATSU, false));
		upseeriRivi2.add(new Nappula(2, 7, white, Tyyppi.LAHETTI, true));
		upseeriRivi2.add(new Nappula(3, 7, white, Tyyppi.KUNINGATAR, false));
		upseeriRivi2.add(new Nappula(4, 7, white, Tyyppi.KUNINGAS, true));
		upseeriRivi2.add(new Nappula(5, 7, white, Tyyppi.LAHETTI, false));
		upseeriRivi2.add(new Nappula(6, 7, white, Tyyppi.RATSU, true));
		upseeriRivi2.add(new Nappula(7, 7, white, Tyyppi.TORNI, false));
		this.lauta.add(upseeriRivi2);

		tulosta();

	}
	
	//tulostusmetodi tulostaa pelilaudan, sekä rivinumerot ja 
	//sarakkeita symboloivat aakkoset
	//
	public void tulosta(){
		String tulosteRivi = "";
		int rivinumero = 1;
		ArrayList<Nappula> rivi = new ArrayList<Nappula>(8);
		System.out.println("    A B C D E F G H");
		System.out.println("  +-----------------+");
		for (int i = 0; i<8; i++) {
			rivi = this.lauta.get(i);
			tulosteRivi += rivinumero;
			tulosteRivi += " |";
			for (Nappula n : rivi) {
				switch(n.annaTyyppi()){
					case TORNI:
						if(n.onkoValkoinen()){
							tulosteRivi += " T";
						}else{
							tulosteRivi += " t";
						}
						break;
					case RATSU:
						if(n.onkoValkoinen()){
							tulosteRivi += " R";
						}else{
							tulosteRivi += " r";
						}
						break;
					case LAHETTI:
						if(n.onkoValkoinen()){
							tulosteRivi += " L";
						}else{
							tulosteRivi += " l";
						}
						break;
					case KUNINGATAR:
						if(n.onkoValkoinen()){
							tulosteRivi += " Q";
						}else{
							tulosteRivi += " q";
						}
						break;
					case KUNINGAS:
						if(n.onkoValkoinen()){
							tulosteRivi += " K";
						}else{
							tulosteRivi += " k";
						}
						break;
					case SOTILAS:
						if(n.onkoValkoinen()){
							tulosteRivi += " S";
						}else{
							tulosteRivi += " s";
						}
						break;
					case RUUTU:
						if(n.onkoValkoinen()){
							tulosteRivi += " #";
						}else{
							tulosteRivi += " 0";
						}
						break;
				}
			}
			tulosteRivi += " |";
			tulosteRivi += rivinumero;
			System.out.println(tulosteRivi);
			rivinumero++;
			tulosteRivi = String.valueOf("");
		}
		System.out.println("  +-----------------+");
		System.out.println("    A B C D E F G H");
	}
	
	//muuttaa kayttajan syottamat koordinaatit 
	public boolean siirto(boolean white, String eka, String toka){
		String firstletter = "";
		String secondletter = "";
		int srcX = 0;
		int srcY = 0;
		int destX = 0;
		int destY = 0;
		Tyyppi t1 = Tyyppi.RUUTU;
		Tyyppi t2 = Tyyppi.RUUTU;

		firstletter = eka.substring(0,1);
		secondletter = eka.substring(1, 2);

		// seuraavat 4 switchia muuttavat koordinaatit vastaamaan pelilautamatriisin 
		// koordinaatteja ja poissulkevat samalla virheelliset koordinaatit
		switch(firstletter){
			case "a":
				srcX = 0;
				break;
			case "b":
				srcX = 1;
				break;
			case "c":
				srcX = 2;
				break;
			case "d":
				srcX = 3;
				break;
			case "e":
				srcX = 4;
				break;
			case "f":
				srcX = 5;
				break;
			case "g":
				srcX = 6;
				break;
			case "h":
				srcX = 7;
				break;
			default:
				return false;	
		}

		switch(secondletter){
			case "1":
				srcY = 0;
				break;
			case "2":
				srcY = 1;
				break;
			case "3":
				srcY = 2;
				break;
			case "4":
				srcY = 3;
				break;
			case "5":
				srcY = 4;
				break;
			case "6":
				srcY = 5;
				break;
			case "7":
				srcY = 6;
				break;
			case "8":
				srcY = 7;
				break;
			default:
				return false;
		}
		firstletter = toka.substring(0,1);
		secondletter = toka.substring(1, 2);

		switch(firstletter){
			case "a":
				destX = 0;
				break;
			case "b":
				destX = 1;
				break;
			case "c":
				destX = 2;
				break;
			case "d":
				destX = 3;
				break;
			case "e":
				destX = 4;
				break;
			case "f":
				destX = 5;
				break;
			case "g":
				destX = 6;
				break;
			case "h":
				destX = 7;
				break;
			default:
				return false;	
		}

		switch(secondletter){
			case "1":
				destY = 0;
				break;
			case "2":
				destY = 1;
				break;
			case "3":
				destY = 2;
				break;
			case "4":
				destY = 3;
				break;
			case "5":
				destY = 4;
				break;
			case "6":
				destY = 5;
				break;
			case "7":
				destY = 6;
				break;
			case "8":
				destY = 7;
				break;
			default:
				return false;
		}
		Nappula n1 = this.lauta.get(srcY).get(srcX);
		Nappula n2 = this.lauta.get(destY).get(destX);
		t1 = n1.annaTyyppi();
		t2 = n2.annaTyyppi();



		//tarkastaa, että valkoisilla pelaava siirtää vain valkoisia ja toisin päin
		if(white != n1.onkoValkoinen()){
			System.out.println("et voi siirtää vastustajan nappuloita");
			return false;
		}
		//tarkastaa, ettei nappulaa siirretä samaan ruutuun, kuin missä se jo on
		if(srcY == destY){
			if (srcX == destX) {
				return false;
			}
		}

		if( n1.siirto(destX, destY)){
			//ratsu on ainoa nappula joka voi hyppia muiden yli, joten estetarkastusta ei tarvita
			if (n1.annaTyyppi() == Tyyppi.RATSU) {
				if (n2.annaTyyppi() != Tyyppi.RUUTU) {
					if (n1.onkoValkoinen() == n2.onkoValkoinen()) {
						System.out.println("Et voi syoda omaa nappulaa");
						return false;
					}else{
						System.out.println("Söit nappulan!");
						muutaTyyppi(n1, n2);
						Nappula s = shakki();
						if(s.annaTyyppi() == Tyyppi.KUNINGAS){
							if(s.onkoValkoinen() == n2.onkoValkoinen()){
								n2.asetaTyyppi(t2);
								n1.asetaTyyppi(t1);
								System.out.println("Et voi siirtaa tata nappulaa");
								return false;
							}
						}
						return true;
					}

				}else{
					System.out.println("heppa hyppii");
					muutaTyyppi(n1, n2);
					Nappula s = shakki();
					if(s.annaTyyppi() == Tyyppi.KUNINGAS){
						if(s.onkoValkoinen() == n2.onkoValkoinen()){
							n2.asetaTyyppi(t2);
							n1.asetaTyyppi(t1);
							System.out.println("Et voi siirtaa tata nappulaa");
							return false;
						}
					}
					return true;
				}
				
			}
			if(n1.annaTyyppi() == Tyyppi.SOTILAS){
				if(n2.annaTyyppi() != Tyyppi.RUUTU){
					System.out.println("sotilas syo vaan vinoon");	
					return false;
				}
			}

			//tarkistetaan onko esteita, ja ovatko ne omia
			if (!onkoEsteita(srcX, srcY, destX, destY)) {
				System.out.println("ei esteita, syodaanko?");
				syonti(n1, n2);
				Nappula s = shakki();
				if(s.annaTyyppi() == Tyyppi.KUNINGAS){
					if(s.onkoValkoinen() == n2.onkoValkoinen()){
						n2.asetaTyyppi(t2);
						n1.asetaTyyppi(t1);
						System.out.println("Et voi siirtaa tata nappulaa");
						return false;
					}
				}

				return true;
			}else{
				return false;
			}
		}else{
			if(n1.annaTyyppi() == Tyyppi.SOTILAS){
				
				if(syonti(n1, n2)){
					Nappula s = shakki();
					if(s.annaTyyppi() == Tyyppi.KUNINGAS){
						if(s.onkoValkoinen() == n2.onkoValkoinen()){
							n2.asetaTyyppi(t2);
							n1.asetaTyyppi(t1);
							System.out.println("Et voi siirtaa tata nappulaa");
							return false;
						}
					}
					return true;
				}
			}

			return false;
		}
	
	}

	//palauttaa false jos esteita ei ole, muuten true
	private boolean onkoEsteita(int srcX, int srcY, int destX, int destY){
		int y = destY;
		int x = destX;
		int liikeX = 0;
		int liikeY = 0;
		
		//liikkuminen vaakasuunnassa
		if(srcY == destY){
			if(srcX < destX){
				//EAST
				liikeX = -1;
			}
			if(srcX > destX){
				//WEST
				liikeX = +1;
			}	
		}
		//liikkuminen pystysuunnassa
		if(srcX == destX){
			if(srcY < destY){
				//SOUTH
				liikeY = -1;
			}
			if(srcY > destY){
				//NORTH
				liikeY = +1;
			}
		}

		if(srcX < destX){
			if(srcY < destY){
				//SOUTHEAST
				liikeX = -1;
				liikeY = -1;
			}
			if(srcY > destY){
				//NORTHEAST
				liikeX = -1;
				liikeY = +1;
			}
		}
		if(srcX > destX){
			if(srcY < destY){
				//SOUTHWEST
				liikeX = +1;
				liikeY = -1;
			}
			if(srcY > destY){
				//NORTHEAST
				liikeX = +1;
				liikeY = +1;
			}
		}

		while(true){
			y = y+liikeY;
			x = x+liikeX;

			if(x == srcX && y == srcY){
				return false;
			}
			if(this.lauta.get(y).get(x).annaTyyppi() != Tyyppi.RUUTU){
				System.out.println("edessa  jotain :D");
				return true;
			}
		}
		
	}

	//siirtaa nappulan uuteen ruutuun ja ilmoittaa syonnin tapahtumisesta
	private boolean syonti(Nappula n1, Nappula n2){
		int srcX = n1.annaX();
		int srcY = n1.annaY();
		int destX = n2.annaX();
		int destY = n2.annaY();
		if(n2.annaTyyppi() != Tyyppi.RUUTU){
			
			if(n1.annaTyyppi() == Tyyppi.SOTILAS){
				if(n1.onkoValkoinen()){
					if(srcY == destY +1){
						if(srcX == destX +1){

							muutaTyyppi(n1, n2);
							return true;
						}
						if(srcX == destX -1){

							muutaTyyppi(n1, n2);
							return true;
						}
					}
				}else{
					if(srcY == destY -1){
						if(srcX == destX +1){

							muutaTyyppi(n1, n2);
							return true;
						}
						if(srcX == destX -1){
	
							muutaTyyppi(n1, n2);
							return true;
						}
					}
				}
			}
			

			muutaTyyppi(n1, n2);
			return true;
		}
		System.out.println("siirtohommia, ei syoda");
		muutaTyyppi(n1, n2);
		return false;
	}

	private void muutaTyyppi(Nappula n1, Nappula n2){
		n2.asetaVari(n1.onkoValkoinen());
		n2.asetaTyyppi(n1.annaTyyppi());
		n1.asetaTyyppi(Tyyppi.RUUTU);
	}

	//parametrina siirrettava nappula, palauttaa kuninkaan jos siirron seurauksena shakki
	//muuten palauttaa ruutunappulan
	public Nappula shakki(){
		Nappula k = new Nappula();
		boolean v = false;
		boolean apu = false;

		for(ArrayList<Nappula> rivi : lauta){
			for(Nappula n : rivi){
				if(n.annaTyyppi() == Tyyppi.KUNINGAS){
					k = n;
					v = k.onkoValkoinen();
					apu = true;
					break;
				}
				if(apu){
					break;
				}
			}
		}

		for(ArrayList<Nappula> rivi : lauta){
			for(Nappula n : rivi){
				if(n.onkoValkoinen() != k.onkoValkoinen()){
					if(n.siirto(k.annaX(), k.annaY())){
						if(!onkoEsteita(n.annaX(), n.annaY(), k.annaX(), k.annaY())){
							System.out.println("SHAKKI!");
							return k;
						}else{
							if(n.annaTyyppi() == Tyyppi.RATSU){
								System.out.println("SHAKKI!");
								return k;
							}
						}
					}
				}
			}
		}

		for(ArrayList<Nappula> rivi : lauta){
			for(Nappula n : rivi){
				if(n.annaTyyppi() == Tyyppi.KUNINGAS){
					if(n.onkoValkoinen() != v){
						k = n;
						v = k.onkoValkoinen();
						apu = true;
						break;
					}
				}
				if(apu){
					break;
				}
			}
		}
		
		for(ArrayList<Nappula> rivi : lauta){
			for(Nappula n : rivi){
				if(n.onkoValkoinen() != k.onkoValkoinen()){
					if(n.siirto(k.annaX(), k.annaY())){
						if(!onkoEsteita(n.annaX(), n.annaY(), k.annaX(), k.annaY())){
							System.out.println("SHAKKI!");
							return k;
						}else{
							if(n.annaTyyppi() == Tyyppi.RATSU){
								System.out.println("SHAKKI!");
								return k;
							}
						}
					}
				}
			}
		}

		return new Nappula();
	}
}
