import java.util.Scanner;
import java.io.*;

public class Main {

	public static void main(String[] args) {
		boolean loop = true;
		Scanner s = new Scanner(System.in);
		
		System.out.println("###Olio-ohjelmoinnin perusteet");
		System.out.println("###Harjoitustyo: Shakkipeli");
		System.out.println("###Antti Auranen");		
		System.out.println("Anna komento:");
		//main loop
		do{
			System.out.printf("> ");
			
			switch(s.nextLine()){
			case "help":
				System.out.println("Komennot:");
				System.out.println("uusi : aloittaa uuden pelin");
				System.out.println("lopeta : poistuu ohjelmasta");
				break;
			case "aloita":
				System.out.println("Kaynnistetaan pelia...");
				peli();
				break;
			case "lopeta":
				loop = false;
				break;
		
				
			}
			
			
		}while(loop);
		s.close();
	}
	
	public static void peli(){
		boolean loop = true;
		boolean valkoinen = true;
		Scanner s = new Scanner(System.in);
		System.out.println("ladataanko vanha peli? (Y/N)");
		System.out.printf("> ");
		Pelilauta lauta = new Pelilauta();
		if(s.nextLine().equals("Y")){
		try{
			FileInputStream saveFile = new FileInputStream("saveFile.sav");
			ObjectInputStream palauta = new ObjectInputStream(saveFile);
				try{
					lauta = (Pelilauta)palauta.readObject();
					lauta.tulosta();
				}catch(IOException e){
					
				}catch(ClassNotFoundException e){
					
				}
				palauta.close();
					
				
				
			
		}catch(IOException e){
			System.err.println("IOException: " + e.getMessage());
		}
		}
		
		do{
			if(valkoinen){
				System.out.println("Valkoisen pelaajan vuoro. ");
			}else{
				System.out.println("Mustan pelaajan vuoro. ");
			}
			System.out.printf("> ");
			
			switch(s.nextLine()){
			case "help":
				System.out.println("Komennot:");
				System.out.println("uusi   : aloittaa uuden pelin");
				System.out.println("lopeta : poistuu ohjelmasta");
				System.out.println("siirto : siirtaa nappulan");
				break;
			case "siirto":
				System.out.println("Anna ensin nappulan koordinaatit, sitten kohderuudun");
				System.out.println("esim: a3 b3");
				System.out.printf("> ");
				
				if(lauta.siirto(valkoinen, s.next(), s.next())){
					System.out.println("Siirto onnistui.");
					lauta.tulosta();
					if(valkoinen){
						valkoinen = false;
					}else{
						valkoinen = true;
					}
				}else{
					System.out.println("Virheellinen siirto!");
				}
				
				break;
			case "lopeta":
				loop = false;
				break;
			case "tulosta":
				lauta.tulosta();
				break;
			case "tallenna":
				if(tallenna(lauta)){
					System.out.println("tallennus onnistui");
				}
				break;

			}
			
		}while(loop);
		s.close();
		
	}

	private static boolean tallenna(Pelilauta lauta){

		try{
			FileOutputStream saveFile = new FileOutputStream("tallennus.sav");
			ObjectOutputStream save = new ObjectOutputStream(saveFile);
			save.writeObject(lauta);
			save.close();
		}catch(IOException e){
			System.err.println("IOException: " + e.getMessage());
			return false;
		}
		return true;
		
	}
	

}
