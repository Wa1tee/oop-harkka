This is a Chess game made by Antti Auranen for Object-oriented programming (olio-ohjelmoinnin perusteet) course.

Apologies for the finnish method and variable names, it looks terrible.

I use this repository as a backup and to learn git version control.

Owner: Antti Auranen, University of Turku

TODO:

-Movement method for knight in class Nappula

-fix bishop and queen movement methods in class Nappula

-fix obstacle checking method

-fix eating method